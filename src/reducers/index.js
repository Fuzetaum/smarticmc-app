import { combineReducers } from 'redux';
import { entities, communication } from 'redux-shelf';

import appReducer from './app';
import userReducer from './user';
import itemReducer from './item';

export default combineReducers({
  app: appReducer,
  user: userReducer,
  item: itemReducer,
  entities,
  communication,
});
