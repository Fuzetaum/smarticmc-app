import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { Col, Layout, Row } from 'antd';

import Header from './components/Header';

import store from './store';
import Routes from './Routes';

import styles from './App.css';

const { Content } = Layout;

class App extends React.PureComponent {
  state = {}

  render() {
    return (
      <Provider store={store}>
        <Router>
          <Layout className={styles.appLayout}>
            <Header/>
            <Layout>
              <Row type="flex" justify="start" align="top" style={{ height: '100%' }}>
                <Col className={styles.cardCol}>
                  <Content style={{ width: '100%', height: '100%' }}>
                    <Routes />
                  </Content>
                </Col>
              </Row>
            </Layout>
          </Layout>
        </Router>
      </Provider>
    );
  }
}

export default App;
