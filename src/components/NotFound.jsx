import React from 'react';
import { Card } from 'antd';

import styles from './NotFound.css';

const NotFound = () => (
  <Card className={styles.card}>
    <p>Page not found!</p>
  </Card>
);

export default NotFound;
