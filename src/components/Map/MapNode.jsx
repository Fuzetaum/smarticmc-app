import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import { setItemAsDestination, setItemAsVisited } from '../../actions/MapActions.js';

const MapNode = ({
  x,
  y,
  coordX,
  coordY,
  isVisited,
  index,
  onSetItemAsDestination,
  onSetItemAsVisited,
}) => {
  function onClickItem() {
    onSetItemAsDestination({
      latitude: coordY,
      longitude: coordX,
      itemId: index + 1,
    });
    onSetItemAsVisited(index);
  }

  return (
    /* eslint-disable jsx-a11y/no-static-element-interactions */
    /* eslint-disable jsx-a11y/click-events-have-key-events */
    <div
      style={{
        position: 'absolute',
        left: x,
        top: y,
        width: '24px',
        height: '24px',
      }}
      onClick={() => { onClickItem(); }}
    >
      <svg style={{ width: '100%', height: '100%' }}>
        <circle
          cx="12"
          cy="12"
          r="10"
          stroke="#444"
          strokeWidth="2"
          fill={isVisited ? '#F00' : '#0F0'}
        />
      </svg>
    </div>
  );
}

MapNode.propTypes = {
  x: PropTypes.string,
  y: PropTypes.string,
  coordX: PropTypes.number,
  coordY: PropTypes.number,
  isVisited: PropTypes.bool,
  index: PropTypes.number.isRequired,
  onSetItemAsDestination: PropTypes.func,
  onSetItemAsVisited: PropTypes.func,
};

MapNode.defaultProps = {
  x: '',
  y: '',
  coordX: 0,
  coordY: 0,
  isVisited: false,
  onSetItemAsDestination: () => {},
  onSetItemAsVisited: () => {},
};

export default connect(
  null,
  {
    onSetItemAsDestination: setItemAsDestination,
    onSetItemAsVisited : setItemAsVisited
  },
)(MapNode);
