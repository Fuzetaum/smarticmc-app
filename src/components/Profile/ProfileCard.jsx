import { Button, Card, Input } from 'antd';
import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { updateUserProfile } from '../../actions/ProfileActions';

import styles from './ProfileCard.css';

const emailValidationRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

class ProfileCard extends React.Component {
  state = {
    email: '',
    emailConfirmation: '',
  }

  onSaveProfile() {
    const { user, onUpdateUserProfile } = this.props;
    const { email, emailConfirmation } = this.state;

    if (email === '' || !emailValidationRegex.test(email)) {
      return;
    }

    if (emailConfirmation === '' || !emailValidationRegex.test(emailConfirmation)) {
      return;
    }

    if(emailConfirmation !== email) {
      return;
    }

    onUpdateUserProfile({
      ...user,
      email,
    });
  }

  onChangeInput(target, value) {
    this.setState({ [target]: value });
  }

  render() {
    const { user } = this.props;
    const { email, emailConfirmation } = this.state;

    if(user.email) {
      return <Redirect to="/map"/>;
    }

    return (
      <Card className={styles.card}>
        <div className={styles.cardBody}>
          <div className={styles.inputContainer}>
            <div className={styles.inputLabelContainer}>
              <p>Usuário:</p>
              <p>E-mail:</p>
              <p>Confirme o e-mail:</p>
            </div>
            <div>
                <Input
                  value={user.username}
                  disabled
                />
                <Input
                  value={email}
                  onChange={event => this.onChangeInput('email', event.target.value)}
                />
                <Input
                  value={emailConfirmation}
                  onChange={event => this.onChangeInput('emailConfirmation', event.target.value)}
                />
              </div>
          </div>
          <Button type="primary" onClick={() => this.onSaveProfile()}>Salvar</Button>
        </div>
      </Card>
    );
  }
}

export default connect(
  ({ user }) => ({ user }),
  {
    onUpdateUserProfile: updateUserProfile,
  },
)(ProfileCard);
