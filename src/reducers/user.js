export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGOUT = 'USER_LOGOUT';
export const USER_UPDATE = 'USER_UPDATE';

const INITIAL_STATE = {
  username: '',
  password: '',
  name: '',
  email: '',
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_LOGIN:
      return action.payload;
    case USER_LOGOUT:
      return INITIAL_STATE;
    case USER_UPDATE:
      return action.payload;
    default:
      return state;
  }
};

export default userReducer;
