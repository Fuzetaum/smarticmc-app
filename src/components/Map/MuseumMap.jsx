import { Button, Card } from 'antd';
import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import MapNode from './MapNode';
import { fetchMuseumItems } from '../../actions/MapActions';

import styles from './MuseumMap.css';

class MuseumMap extends React.Component {
  state = {
    exit: false,
  }

  componentDidMount() {
    this.props.onFetchMuseumItems();
  }

  renderPathStrokes(limitCoordinates, growthXFactor, growthYFactor) {
    const { path } = this.props.item;
    return (
      <svg style={{
        width: '100%', height: '100%'
      }}>
        {/* Path strokes */}
        {path.pathEdges.map(edge => {
          const vertices = edge.split('-');
          return <line
            x1={`${5 + (path.pathNodes[vertices[0]-1].x - limitCoordinates.minX) * growthXFactor}%`}
            y1={`${5 + (path.pathNodes[vertices[0]-1].y - limitCoordinates.minY) * growthYFactor}%`}
            x2={`${5 + (path.pathNodes[vertices[1]-1].x - limitCoordinates.minX) * growthXFactor}%`}
            y2={`${5 + (path.pathNodes[vertices[1]-1].y - limitCoordinates.minY) * growthYFactor}%`}
            style={{ stroke: '#000', strokeWidth: '2' }}
          />;
        })}
      </svg>
    );
  }

  renderPathNodes = (limitCoordinates, growthXFactor, growthYFactor) => {
    const { item, visited } = this.props;
    const { items } = item;
    return items.map((vertex, index) =>
      <MapNode
        x={`${5 + ((vertex.x - limitCoordinates.minX) * growthXFactor)}%`}
        y={`${5 + ((vertex.y - limitCoordinates.minY) * growthYFactor)}%`}
        coordX={vertex.x}
        coordY={vertex.y}
        isVisited={visited.includes(index)}
        index={index}
      />
    );
  }

  render() {
    const { item } = this.props;
    const { path, items } = item;

    if (!path || path.pathNodes.length < 1) {
      return null;
    }

    const gapX = 90;
    const gapY = 90;
    const limitCoordinates = [...items, ...path.pathNodes].reduce((acc, vertex) => {
      return {
        minX: (vertex.x < acc.minX) ? vertex.x : acc.minX,
        maxX: (vertex.x > acc.maxX) ? vertex.x : acc.maxX,
        minY: (vertex.y < acc.minY) ? vertex.y : acc.minY,
        maxY: (vertex.y > acc.maxY) ? vertex.y : acc.maxY,
      };
    }, {
      minX: Number.MAX_SAFE_INTEGER,
      maxX: Number.MIN_SAFE_INTEGER,
      minY: Number.MAX_SAFE_INTEGER,
      maxY: Number.MIN_SAFE_INTEGER,
    });
    const growthXFactor = gapX / (limitCoordinates.maxX - limitCoordinates.minX);
    const growthYFactor = gapY / (limitCoordinates.maxY - limitCoordinates.minY);

    if (this.props.destination.latitude) {
      return <Redirect to="/compass"/>;
    }

    if (this.state.exit) {
      return <Redirect to="/logout"/>;
    }

    return (
      <Card
        className={styles.card}
        bodyStyle={{ position: 'absolute', left: '0', top: '15%', bottom: '0', right: '0', }}
        title={
          <div className={styles.cardHeader}>
            <Button
              size="large"
              type="primary"
              onClick={() => this.setState({ exit: true })}
            >Encerrar Visita</Button>
          </div>
        }
      >
        {this.renderPathStrokes(limitCoordinates, growthXFactor, growthYFactor)}
        {this.renderPathNodes(limitCoordinates, growthXFactor, growthYFactor)}
      </Card>
    );
  }
}

export default connect(
  ({ app, item }) => ({
    item,
    destination: app.destination,
    visited: app.visited,
  }),
  {
    onFetchMuseumItems: fetchMuseumItems,
  },
)(MuseumMap);
