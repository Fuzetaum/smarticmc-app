import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Card } from 'antd';
import { Link } from 'react-router-dom';

import { endUserSession } from '../../actions/LoginActions';
import { flushItemDestination, flushVisitedItemList } from '../../actions/MapActions';
import { registerUserSession } from '../../actions/ProfileActions';
import { flushItemLikes } from '../../actions/ItemActions';

import styles from './LogoutConfirm.css';

const LogoutConfirm = ({
  user,
  liked,
  visited,
  onEndUserSession,
  onFlushItemDestination,
  onFlushItemLikes,
  onFlushVisitedItemList,
}) => {
  return (
    <Card className={styles.card}>
      <div className={styles.cardBody}>
        <p>Você deseja mesmo encerrar a visita ao SmartICMC?</p>
        <Link to="/">
          <Button type="primary" onClick={() => {
            registerUserSession(user, visited, liked);
            onFlushItemDestination();
            onFlushItemLikes();
            onFlushVisitedItemList();
            onEndUserSession();
          }}>Sim</Button>
        </Link>
        <Link to="/map">
          <Button type="danger">Não</Button>
        </Link>
      </div>
    </Card>
  );
}

LogoutConfirm.propTypes = {
  user: PropTypes.shape(),
  liked: PropTypes.arrayOf(PropTypes.number),
  visited: PropTypes.arrayOf(PropTypes.number),
  onEndUserSession: PropTypes.func,
  onFlushItemDestination: PropTypes.func,
  onFlushItemLikes: PropTypes.func,
  onFlushVisitedItemList: PropTypes.func,
};

LogoutConfirm.defaultProps = {
  user: {},
  liked: [],
  visited: [],
  onEndUserSession: () => {},
  onFlushItemDestination: () => {},
  onFlushItemLikes: () => {},
  onFlushVisitedItemList: () => {},
};

export default connect(
  ({ app, user }) => ({
    user,
    liked: app.liked,
    visited: app.visited,
  }),
  {
    onEndUserSession: endUserSession,
    onFlushItemDestination: flushItemDestination,
    onFlushItemLikes: flushItemLikes,
    onFlushVisitedItemList: flushVisitedItemList,
  },
)(LogoutConfirm);
