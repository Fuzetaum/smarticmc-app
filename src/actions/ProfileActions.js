import axios from 'axios';
import { communication } from 'redux-shelf';

import { USER_UPDATE } from '../reducers/user';

import { USER_TYPE_ENTITY } from './LoginActions';

export function updateUserProfile(userData) {
  return async (dispatch) => {
    dispatch(communication.starting(USER_TYPE_ENTITY));

    try {
      dispatch({
        type: USER_UPDATE,
        payload: {
          ...userData,
        }
      });

      dispatch(communication.done(USER_TYPE_ENTITY));
    } catch (error) {
      dispatch(communication.fail(USER_TYPE_ENTITY, error));
    }
  };
}

export function createUser(userData) {
  return async (dispatch) => {
    dispatch(communication.starting(USER_TYPE_ENTITY));

    try {
      const response = await axios.post(`${window.ENV.BACKEND_URL}/user/create`, {
        ...userData,
      });
      console.log(response.data);

      dispatch(communication.done(USER_TYPE_ENTITY));
    } catch (error) {
      dispatch(communication.fail(USER_TYPE_ENTITY, error));
    }
  };
}

export async function registerUserSession(user, visitedItems, likedItems) {
  if (visitedItems.length === 0) {
    return;
  }

  const itinerary = visitedItems.reduce((acc, item) => {
    if (acc === '') {
      return `${item}`;
    }
    return acc.concat(`-${item}`);
  }, '');
  const liked = likedItems.reduce((acc, item) => {
    if (acc === '') {
      return `${item}`;
    }
    return acc.concat(`-${item}`);
  }, '');
  await axios.post(`${window.ENV.BACKEND_URL}/user/save-session`, {
    username: user.username,
    itinerary,
    liked,
  });
}
