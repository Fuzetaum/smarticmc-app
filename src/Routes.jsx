import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Compass from './components/Compass/Compass';
import CreateUser from './components/Login/AccessManagement/CreateUser';
import ItemCard from './components/ItemCard/ItemCard';
import LoginCard from './components/Login/LoginCard';
import MuseumMap from './components/Map/MuseumMap';
import NotFound from './components/NotFound';
import ProfileCard from './components/Profile/ProfileCard';
import RecoverPassword from './components/Login/AccessManagement/RecoverPassword';
import LogoutConfirm from './components/LogoutConfirm/LogoutConfirm';

const Routes = () => (
  <Switch>

    <Route
      exact
      path="/recover-password"
      component={RecoverPassword}
    />

    <Route
      exact
      path="/create-user"
      component={CreateUser}
    />

    <Route
      exact
      path="/profile"
      component={ProfileCard}
    />

    <Route
      exact
      path="/map"
      component={MuseumMap}
    />

    <Route
      exact
      path="/compass"
      component={Compass}
    />

    <Route
      exact
      path="/item"
      component={ItemCard}
    />

    <Route
      exact
      path="/logout"
      component={LogoutConfirm}
    />

    <Route
      exact
      path="/"
      component={LoginCard}
      // component={CreateUser}
    />

    <Route
      exact
      component={NotFound}
    />
  </Switch>
);

export default Routes;
