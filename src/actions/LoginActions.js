import axios from 'axios';
import { communication } from 'redux-shelf';

import { USER_LOGIN, USER_LOGOUT } from '../reducers/user';

export const USER_TYPE_ENTITY = 'user';

export function fetchUserSession(username, password) {
  return async (dispatch) => {
    dispatch(communication.starting(USER_TYPE_ENTITY));

    try {
      const { data } = await axios.post(`${window.ENV.BACKEND_URL}/user/login`, {
        username,
        password,
      }, {
        'Access-Control-Allow-Origin': '*',
      });

      if (data.message) {
        const userData = await axios.post(`${window.ENV.BACKEND_URL}/user/get`, {
          username,
          password,
        }, {
          'Access-Control-Allow-Origin': '*',
        });

        dispatch({
          type: USER_LOGIN,
          payload: { ...userData.data.message },
        });
      }

      dispatch(communication.done(USER_TYPE_ENTITY));
    } catch (error) {
      console.error('Error when trying to log user in:', error);
      dispatch(communication.fail(USER_TYPE_ENTITY, error));
    }
  };
}

export function endUserSession() {
  return async (dispatch) => {
    dispatch({ type: USER_LOGOUT });
  };
}
