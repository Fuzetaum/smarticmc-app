import React from 'react';
import { Layout } from 'antd';

import styles from './Header.css';

const { Header: AntHeader } = Layout;

const Header = () => (
  <AntHeader className={styles.header}>
    <p>SmartICMC</p>
  </AntHeader>
);

export default Header;
