import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Card } from 'antd';

import styles from './CreateUser.css';

class RecoverPassword extends React.Component {
  state = {
    username: '',
  }

  onChangeInput(target, value) {
    this.setState({ [target]: value });
  }

  render() {
    return (
      <Card className={styles.card}>
        <div className={styles.cardBody}>
          {/* <InputField
            inputLabel="Usuário:"
            value={this.state.username}
            onChangeInput={value => this.onChangeInput('username', value)}
          /> */}
          <Link to="/">
            <Button type="primary">OK</Button>
          </Link>
          <Link to="/">
            <Button type="danger">Cancelar</Button>
          </Link>
        </div>
      </Card>
    );
  }
}

export default RecoverPassword;
