import axios from 'axios';
import { communication } from 'redux-shelf';

import {
  APP_DESTINATION_FLUSH,
  APP_DESTINATION_SET,
  APP_VISITED,
  APP_VISITED_FLUSH,
} from '../reducers/app';
import { ITEM_GET } from '../reducers/item';

export const MUSEUM_ITEM_ENTITY = 'item';

export function fetchMuseumItems() {
  return async (dispatch) => {
    dispatch(communication.starting(MUSEUM_ITEM_ENTITY));

    try {
      const pathResponse = await axios.get(`${window.ENV.BACKEND_URL}/map/path/get`);
      const itemResponse = await axios.get(`${window.ENV.BACKEND_URL}/map/item/get`);
      const pathList = generatePathNodeObject(pathResponse.data.message);
      const itemList = generateItemNodeArray(itemResponse.data.message);

      dispatch({
        type: ITEM_GET,
        payload: {
          ...pathList,
          items: itemList,
        },
      });

      dispatch(communication.done(MUSEUM_ITEM_ENTITY));
    } catch (error) {
      dispatch(communication.fail(MUSEUM_ITEM_ENTITY, error));
    }
  };
}

export function setItemAsDestination(mapItem) {
  return async (dispatch) => {
    dispatch({
      type: APP_DESTINATION_SET,
      payload: {
        destination: {
          latitude: mapItem.latitude,
          longitude: mapItem.longitude,
          itemId: mapItem.itemId,
        },
      },
    });
  };
}

export function flushItemDestination() {
  return async (dispatch) => {
    dispatch({
      type: APP_DESTINATION_FLUSH,
    });
  };
}

export function setItemAsVisited(mapItem) {
  return async (dispatch) => {
    dispatch({
      type: APP_VISITED,
      payload: {
        item: mapItem,
      },
    });
  };
}

export function flushVisitedItemList() {
  return async (dispatch) => {
    dispatch({ type: APP_VISITED_FLUSH });
  };
}

const generatePathNodeObject = (pathNodeList) => {
  const returnObject = {
    path: {
      pathNodes: pathNodeList.map((node) => {
        return {
          id: node.id,
          x: node.longitude,
          y: node.latitude,
        };
      }),
      pathEdges: pathNodeList.reduce((acc, node) => {
        const edgeList = node.edges.split(',');
        return [...acc, ...edgeList.map((edgeNode) => {
          const edge = (node.id < Number.parseInt(edgeNode)) ?
            `${node.id}-${edgeNode}` : `${edgeNode}-${node.id}`;
          if (!acc.includes(edge)) return edge;
          return '';
        }).filter(edge => edge ? true : false)];
      }, []),
    }
  };

  return returnObject;
}

const generateItemNodeArray = (itemNodeList) => {
  const idOffset = itemNodeList[0].id - 1;
  return itemNodeList.map((node) => {
    return {
      id: node.id - idOffset,
      x: node.longitude,
      y: node.latitude,
    };
  });
}
