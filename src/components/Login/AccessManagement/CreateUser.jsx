import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, Card, Input } from 'antd';

import { createUser } from '../../../actions/ProfileActions';

import styles from './CreateUser.css';

class CreateUser extends React.Component {
  state = {
    username: '',
    password: '',
    passwordConfirm: '',
  }

  onChangeInput(target, value) {
    this.setState({ [target]: value });
  }

  onConfirmUserCreation() {
    const { username, password, passwordConfirm } = this.state;
    const { onCreateUser } = this.props;

    if (password !== passwordConfirm) {
      return;
    }

    onCreateUser({
      username,
      password,
    });
  }

  render() {
    return (
      <Card className={styles.card}>
        <div className={styles.cardBody}>
          <div className={styles.inputContainer}>
            <div className={styles.inputLabelContainer}>
              <p>Usuário:</p>
              <p>Senha:</p>
              <p>Confirme a senha:</p>
            </div>
            <div>
              <Input
                value={this.state.username}
                onChange={event => this.onChangeInput('username', event.target.value)}
              />
              <Input
                value={this.state.password}
                type="password"
                onChange={event => this.onChangeInput('password', event.target.value)}
              />
              <Input
                value={this.state.passwordConfirm}
                type="password"
                onChange={event => this.onChangeInput('passwordConfirm', event.target.value)}
              />
            </div>
          </div>
          <Link to="/">
            <Button type="primary" onClick={() => this.onConfirmUserCreation()}>OK</Button>
          </Link>
          <Link to="/">
            <Button type="danger">Cancelar</Button>
          </Link>
        </div>
      </Card>
    );
  }
}

export default connect(
  null,
  {
    onCreateUser: createUser,
  },
)(CreateUser);
