import axios from 'axios';

import {
  APP_LIKE,
  APP_LIKE_FLUSH,
  APP_UNLIKE,
} from '../reducers/app';

export async function fetchItemData(itemId) {
  const response = await axios.get(`${window.ENV.BACKEND_URL}/item/get`, { params: { id: itemId } });
  const { name, description, image, amountofLikes } = response.data.message;
  return {
    name,
    description,
    image,
    likes: amountofLikes,
  };
};

export function toggleItemLike(itemId, isLiked) {
  return async (dispatch) => {
    if (isLiked) {
      dispatch({
        type: APP_LIKE,
        payload: { itemId },
      });
    } else {
      dispatch({
        type: APP_UNLIKE,
        payload: { itemId },
      });
    }
  };
};

export function flushItemLikes() {
  return async (dispatch) => {
    dispatch({ type: APP_LIKE_FLUSH });
  };
}
