import { Card } from 'antd';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import ARROW from '../../assets/arrow.svg';

import styles from './Compass.css';

class Compass extends React.Component {
  state = {
    absolute: false,
    alpha: 0,
    latitude: 0,
    longitude: 0,
    distance: Number.MAX_SAFE_INTEGER,
  }

  calculateDistanceFromDestination = (longitude, latitude) => {
    const { destination } = this.props;
    const R = 6371e3; // meters
    const φ1 = latitude.toRadians();
    const φ2 = destination.latitude.toRadians();
    const Δφ = (destination.latitude-latitude).toRadians();
    const Δλ = (destination.longitude-longitude).toRadians();

    const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ/2) * Math.sin(Δλ/2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    return R * c;
  }

  componentDidMount() {
    window.addEventListener('deviceorientation', this.handleOrientationChange);
    setInterval(() => {
      navigator.geolocation.getCurrentPosition(position => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          distance: this.calculateDistanceFromDestination(position.coords.longitude, position.coords.latitude)
        });
      });
    }, 1000);
  }

  handleOrientationChange = (event) => {
    this.setState({
      absolute: event.absolute,
      alpha: event.alpha,
    });
  }

  render() {
    const { destination } = this.props;
    const {
      // Alpha is the horizontal angle of view of the user
      alpha,
      distance,
      latitude,
      longitude,
    } = this.state;

    const azimuth = Math.atan(
      Math.abs((latitude - destination.latitude)/(longitude - destination.longitude)));
    const sightAngle = (alpha > 180 ? 360 - alpha : alpha) * Math.PI / 180;

    if (distance <= window.ENV.PROXIMITY_THRESHOLD) {
      return <Redirect to="/item" />;
    }

    return (
      <Card className={styles.card}>
        <div className={styles.cardBody}>
          <img
            src={ARROW}
            className={styles.arrow}
            style={{
              transform: `rotate(${azimuth - sightAngle}rad)`,
            }}
          />
        </div>
        <p>Distance: {distance}</p>
      </Card>
    );
  }
}

Compass.propTypes = {
  destination: PropTypes.shape(),
};

Compass.defaultProps = {
  destination: {},
};

export default connect(
  ({ app }) => ({
    destination: app.destination,
  })
)(Compass);
