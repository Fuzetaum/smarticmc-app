export const APP_DESTINATION_SET = 'APP_DESTINATION_SET';
export const APP_DESTINATION_FLUSH = 'APP_DESTINATION_FLUSH';
export const APP_VISITED = 'APP_VISITED';
export const APP_VISITED_FLUSH = 'APP_VISITED_FLUSH';
export const APP_LIKE = 'APP_LIKE';
export const APP_UNLIKE = 'APP_UNLIKE';
export const APP_LIKE_FLUSH = 'APP_LIKE_FLUSH';

const INITIAL_STATE = {
  destination: {},
  visited: [],
  liked: [],
};

const appReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case APP_DESTINATION_SET:
      return {
        ...state,
        destination: action.payload.destination,
      };
    case APP_DESTINATION_FLUSH:
      return {
        ...state,
        destination: {},
      };
    case APP_LIKE:
      if (state.liked.includes(action.payload.itemId)) {
        return state;
      }
      return {
        ...state,
        liked: [...state.liked, action.payload.itemId],
      };
    case APP_LIKE_FLUSH:
      return {
        ...state,
        liked: [],
      };
    case APP_UNLIKE:
      return {
        ...state,
        liked: state.liked.filter(item => item !== action.payload.itemId),
      };
    case APP_VISITED:
      return {
        ...state,
        visited: (state.visited.includes(action.payload.item) ?
          state.visited
          : [...state.visited, action.payload.item]),
      };
    case APP_VISITED_FLUSH:
      return {
        ...state,
        visited: [],
      };
    default:
      return state;
  }
};

export default appReducer;
