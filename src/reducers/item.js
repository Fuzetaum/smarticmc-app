export const ITEM_GET = 'ITEM_GET';
export const ITEM_FLUSH = 'ITEM_FLUSH';

const INITIAL_STATE = {
  vertexList: [],
  edgeList: [],
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ITEM_GET:
      return action.payload;
    case ITEM_FLUSH:
      return INITIAL_STATE;
    default:
      return state;
  }
};

export default userReducer;
