import React from 'react';
import PropTypes from 'prop-types';
import { Button, Card, Icon, Spin } from 'antd';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { flushItemDestination } from '../../actions/MapActions';
import { fetchItemData, toggleItemLike } from '../../actions/ItemActions';

import styles from './ItemCard.css';

class ItemCard extends React.Component {
  state = {
    ready: false,
    name: '',
    description: '',
    image: '',
    likes: 0,
    isLiked: false,
  }

  async componentDidMount() {
    const { itemId } = this.props;
    if (itemId > -1) {
      const { name, description, image, likes } = await fetchItemData(itemId);
      this.setState({
        name,
        description,
        image,
        likes,
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.liked && !prevState.isLiked) {
      this.setState({ isLiked: true });
    }
  }

  render() {
    const { liked, itemId, onFlushItemDestination, onToggleItemLike } = this.props;
    const { name, description, image, ready, likes, isLiked } = this.state;

    if (ready) {
      return <Redirect to="/map" />;
    }

    if (name === '') {
      return (
        <Card
        className={styles.card}
        title={
          <div className={styles.cardHeader}>
            <Button
              size="large"
              type="primary"
            >Pronto</Button>
          </div>
        }
      >
        <div className={styles.cardBody}>
          <Spin />
        </div>
      </Card>
      );
    }

    return (
      <Card
        className={styles.card}
        title={
          <div className={styles.cardHeader}>
            <div>
              <p>{name.toUpperCase()}</p>
            </div>
            <div style={{ display: 'flex' }}>
              <Button
                type={isLiked ? 'primary' : 'default'}
                onClick={() => this.setState({ isLiked: !isLiked })}
              ><Icon type="like" /></Button>
              <p>Likes: {likes}</p>
            </div>
            <Button
              size="large"
              type="primary"
              onClick={() => {
                onToggleItemLike(itemId, isLiked);
                onFlushItemDestination();
                this.setState({ ready: true });
              }}
            >Pronto</Button>
          </div>
        }
      >
        <div className={styles.cardBody}>
          <img src={image} alt="Tree"/>
          <p>{description}</p>
        </div>
      </Card>
    );
  }
}

ItemCard.propTypes = {
  itemId: PropTypes.number,
  liked: PropTypes.bool,
  onFlushItemDestination: PropTypes.func,
  onToggleItemLike: PropTypes.func,
};

ItemCard.defaultProps = {
  itemId: -1,
  liked: false,
  onFlushItemDestination: () => {},
  onToggleItemLike: () => {},
};

export default connect(
  ({ app }) => ({
    itemId: app.destination.itemId,
    liked: app.liked.includes(app.destination.itemId),
  }),
  {
    onFlushItemDestination: flushItemDestination,
    onToggleItemLike: toggleItemLike,
  },
)(ItemCard);
