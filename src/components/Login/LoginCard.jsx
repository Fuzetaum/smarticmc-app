import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Card, Input } from 'antd';
import { Link, Redirect } from 'react-router-dom';

import { USER_TYPE_ENTITY, fetchUserSession } from '../../actions/LoginActions';

import styles from './LoginCard.css';

class LoginCard extends React.Component {
  state = {
    username: '',
    password: '',
  }

  componentWillReceiveProps(nextProps) {
    if (Object.keys(nextProps.user).length > 0) {
      const { username, password } = nextProps.user;
      this.setState({
        username,
        password,
      });
    }
  }

  onChangeInput(target, value) {
    this.setState({ [target]: value });
  }

  render() {
    const {
      loading,
      error,
      onFetchUserSession,
      user,
    } = this.props;
    const { username, password } = this.state;

    if (loading) {
      return null;
    }

    if (Object.keys(error).length > 0) {
      return null;
    }

    if (user.username !== '' && user.password !== '') {
      if (user.email !== '') {
        return <Redirect to="/map"/>;
      }
      return <Redirect to="/profile"/>;
    }

    return (
      <Card className={styles.card}>
        <div className={styles.cardBody}>
          <div className={styles.inputContainer}>
            <div className={styles.inputLabelContainer}>
              <p>Usuário:</p>
              <p>Senha:</p>
            </div>
            <div>
              <Input
                value={this.state.username}
                onChange={event => this.onChangeInput('username', event.target.value)}
              />
              <Input
                value={this.state.password}
                type="password"
                onChange={event => this.onChangeInput('password', event.target.value)}
              />
            </div>
          </div>
          <Button type="primary" onClick={() => onFetchUserSession(username, password)}>Entrar</Button>
          <div>
            <Link to="/recover-password">
              <Button size="small">Recuperar Senha</Button>
            </Link>
            <Link to="/create-user">
              <Button size="small">Primeiro Acesso</Button>
            </Link>
          </div>
        </div>
      </Card>
    );
  }
}

LoginCard.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.shape({}),
  user: PropTypes.shape({
    username: PropTypes.string,
    password: PropTypes.string,
    email: PropTypes.string,
  }),
  onFetchUserSession: PropTypes.func,
};

LoginCard.defaultProps = {
  loading: false,
  error: {},
  user: {
    username: '',
    password: '',
    email: '',
  },
  onFetchUserSession: () => {},
};

export default connect(
  ({ user, communication }) => {
    const { loading, error } = communication.of(USER_TYPE_ENTITY);
    return {
      loading,
      error,
      user,
    };
  },
  {
    onFetchUserSession: fetchUserSession,
  },
)(LoginCard);
